package upsud.hci909.lab2;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.net.URL;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.border.BevelBorder;

public class PhototequeView {

	private JFrame phototequeFrame;
	private JMenuBar menubar;
	private JMenu fileMenu;
	private JMenuItem menuItemImport;
	private JToolBar toolbar;
	private PhototequeController controller;
	private JPanel topLevelPanel;
	private JLabel statusLabel;
	private JMenuItem menuItemDelete;
	private JMenuItem menuItemQuit;
	private JMenu viewMenu;
	private JMenuItem menuItemPhoto;
	private JMenuItem menuItemBrowser;
	private JMenuItem menuItemSplit;
	private PhototequeComponentModel ptComponentModel;
	private PhototequeComponent ptComponent;
	private JPanel centralPanel;
	private JScrollPane scrollPane;
	private JToggleButton boldToggleButton;
	private JButton underlineToggleButton;
	private JButton brushButton;

	public PhototequeView(PhototequeModel ptmodelInstance) {

		phototequeFrame = new JFrame(ptmodelInstance.TITLE);
		phototequeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		phototequeFrame.setSize(ptmodelInstance.getFrameDesfaultSize());

		// Create Menu bar
		menubar = new JMenuBar();
		populateMenuBar(menubar);
		phototequeFrame.setJMenuBar(menubar);

		// Create Toolbar
		toolbar = new JToolBar();
		toolbar.setFloatable(false);
		populateToolBar(toolbar);
		updateEditButtonsStates(false);

		// Main JPanel to host the custom JComponent
		centralPanel = new JPanel();
		centralPanel.setLayout(new BoxLayout(centralPanel, BoxLayout.Y_AXIS));
		centralPanel.setBackground(ptmodelInstance.getBackgroundColor());

		scrollPane = new JScrollPane(centralPanel);
		scrollPane.setVisible(true);

		// Create status bar
		statusLabel = new JLabel("Loading...");
		statusLabel.setBorder(new BevelBorder(BevelBorder.LOWERED));

		topLevelPanel = new JPanel(new BorderLayout());
		topLevelPanel.setOpaque(true);
		topLevelPanel.add(toolbar, BorderLayout.NORTH);
		topLevelPanel.add(scrollPane, BorderLayout.CENTER);
		topLevelPanel.add(statusLabel, BorderLayout.SOUTH);

		phototequeFrame.add(topLevelPanel);
		phototequeFrame.setVisible(true);
	}

	private void populateToolBar(JToolBar tb) {
		JButton button = null;

		// first button
		button = makeNavigationButton("home24", "home", "Back to main page", "Home");
		button.addActionListener(e -> {
			controller.triggerHomeButton();
		});
		tb.add(button);

		// second button
		button = makeNavigationButton("back24", "previous", "Back to previous something-or-other", "Previous");
		button.addActionListener(e_ -> {
			controller.triggerBackButton();
		});
		tb.add(button);

		// third button
		button = makeNavigationButton("forward24", "next", "Forward to something-or-other", "Next");
		button.addActionListener(e_ -> {
			controller.triggerNextButton();
		});
		tb.add(button);

		tb.addSeparator();

		JToggleButton toggleButton = new JToggleButton();
		// Front side
		String imgDefaultIconLocation = "/vacation24.png";
		URL imgDefaultIconURL = getClass().getResource(imgDefaultIconLocation);
		if (imgDefaultIconURL != null) {
			toggleButton.setIcon(new ImageIcon(imgDefaultIconURL, "Vacation"));
		} else {
			toggleButton.setText("Vacation");
			System.err.println("Resource not found: " + imgDefaultIconLocation);
		}
		// Back side
		String imgSelectedIconLocation = "/family24.png";
		URL imgSelectedIconURL = getClass().getResource(imgDefaultIconLocation);
		if (imgSelectedIconURL != null) {
			toggleButton.setSelectedIcon(new ImageIcon(imgSelectedIconURL, "Vacation"));
		} else {
			toggleButton.setText("Vacation");
			System.err.println("Resource not found: " + imgSelectedIconLocation);
		}
		toggleButton.setToolTipText("Vacation");
		toggleButton.addActionListener(e -> {
			controller.triggerToggleButton();
		});
		tb.add(toggleButton);
		
		tb.addSeparator();
//		
//		button = makeNavigationButton("font24", "change font", "Set desired font", "Font Type");
//		button.addActionListener(e -> {
//			// TODO make it do something
//			//controller.triggerFontType();
//		});
//		tb.add(button);
		
		boldToggleButton = new JToggleButton();
		String boldButtonIconLocation = "/font_bold24.png";
		URL boldButtonIconURL = getClass().getResource(boldButtonIconLocation);
		if (imgDefaultIconURL != null) {
			boldToggleButton.setIcon(new ImageIcon(boldButtonIconURL));
		} else {
			boldToggleButton.setText("Bold");
			System.err.println("Resource not found: " + imgDefaultIconLocation);
		}
		boldToggleButton.setToolTipText("Bold");
		boldToggleButton.addActionListener( e -> {
			controller.triggerFontBold(e);
			
		});
		tb.add(boldToggleButton);
		
		underlineToggleButton = makeNavigationButton("font_underline24", "Underline", "Underline", "Underline");
		underlineToggleButton.addActionListener( e -> {
			// TODO make it do something
			//controller.triggerFontUnderline(e);
		});
		
		tb.add(underlineToggleButton);
		
		tb.addSeparator();
		
		brushButton = makeNavigationButton("brush24", "Brush size", "Change brush size", "Brush Size");
		brushButton.addActionListener( e -> {
			// TODO make it do something
			//controller.triggerBrushSize;
		});
		tb.add(brushButton);
		
		
	}

	private JToggleButton makeToggleButton(String iconFileName, String actionCommand, String toolTipText) {
		JToggleButton jtb = new JToggleButton();
		String imgDefaultIconLocation = String.format("/%s.png", iconFileName);
		URL imgDefaultIconURL = getClass().getResource(imgDefaultIconLocation);
		if (imgDefaultIconURL != null) {
			jtb.setIcon(new ImageIcon(imgDefaultIconURL));
		} else {
			jtb.setText(actionCommand);
			System.err.println("Resource not found: " + imgDefaultIconLocation);
		}
		jtb.setToolTipText(toolTipText);
		jtb.setEnabled(false);
		
		return jtb;
	}

	private JButton makeNavigationButton(String imageFileName, String actionCommand, String toolTipText,
			String altText) {

		String imgLocation = String.format("/%s.png", imageFileName);
		URL imageURL =  PhototequeView.class.getResource(imgLocation);

		// Create and initialize the button.
		JButton button = new JButton();
		button.setActionCommand(actionCommand);
		button.setToolTipText(toolTipText);

		if (imageURL != null) {
			button.setIcon(new ImageIcon(imageURL, altText));
		} else {
			button.setText(altText);
			System.err.println("Resource not found: " + imgLocation);
		}

		return button;
	}

	private void populateMenuBar(JMenuBar mb) {
		fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menubar.add(fileMenu);

		menuItemImport = new JMenuItem("Import", KeyEvent.VK_I);
		menuItemImport.addActionListener(e -> {
			controller.triggerFileImport(e);
		});
		fileMenu.add(menuItemImport);

		menuItemDelete = new JMenuItem("Delete", KeyEvent.VK_D);
		menuItemDelete.addActionListener(e -> {
			controller.triggerFileDelete(e);
		});
		fileMenu.add(menuItemDelete);

		menuItemQuit = new JMenuItem("Quit", KeyEvent.VK_Q);
		menuItemQuit.addActionListener(e -> {
			controller.triggerFileQuit();
		});
		fileMenu.add(menuItemQuit);

		// Create `View` menu
		viewMenu = new JMenu("View");
		viewMenu.setMnemonic(KeyEvent.VK_V);
		fileMenu.getAccessibleContext().setAccessibleDescription("View menu");
		menubar.add(viewMenu);

		menuItemPhoto = new JMenuItem("Photo Viewer", KeyEvent.VK_P);
		menuItemPhoto.addActionListener(e -> {
			controller.triggerViewPhotoViewer(e);
		});
		viewMenu.add(menuItemPhoto);

		menuItemBrowser = new JMenuItem("Browser", KeyEvent.VK_B);
		menuItemBrowser.addActionListener(e -> {
			controller.triggerViewBrowser(e);
		});
		viewMenu.add(menuItemBrowser);

		menuItemSplit = new JMenuItem("Split Mode", KeyEvent.VK_S);
		menuItemSplit.addActionListener(e -> {
			controller.triggerViewSplitMode(e);
		});
		viewMenu.add(menuItemSplit);

	}

	public void setController(PhototequeController controller) {
		this.controller = controller;
	}

	public JLabel getStatusLabel() {
		return statusLabel;
	}

	public PhototequeComponent getPhototequeComponent() {
		return ptComponent;
	}
	
	public void removePhototequeComponent() {
		centralPanel.remove(ptComponent);
		updateEditButtonsStates(false);
		phototequeFrame.repaint();
	}

	private void updateEditButtonsStates(boolean status) {
		boldToggleButton.setEnabled(status);
		underlineToggleButton.setEnabled(status);
		brushButton.setEnabled(status);
		phototequeFrame.repaint();
		
	}

	public void setPhototequeComponent(String filePath) {
		if (centralPanel.getComponentCount() > 0)
			centralPanel.removeAll();
		ptComponentModel = new PhototequeComponentModel(filePath);
		ptComponent = new PhototequeComponent(ptComponentModel);
		Box lineBox = new Box(BoxLayout.LINE_AXIS);
		lineBox.add(Box.createHorizontalGlue());
		lineBox.add(ptComponent);
		centralPanel.add(Box.createVerticalGlue());
		centralPanel.add(lineBox);
		updateEditButtonsStates(true);
		phototequeFrame.repaint();
	}
	
	public PhototequeComponentModel getPhototequeComponentModel() {
		return ptComponentModel;
	}

}
