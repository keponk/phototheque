package upsud.hci909.lab2;

import java.awt.Color;
import java.awt.Dimension;

public class PhototequeModel {
	
	public final String TITLE = "Phototeque Lab 2"; 
	public final class UpdateMessages {
		public static final String HOME_CLICK = "Home button clicked.";
		public static final String BACK_CLICK = "Back button clicked.";
		public static final String FORWARD_CLICK = "Forward button clicked.";
		public static final String TOGGLE_CLICK = "Toggle button clicked.";
		public static final String UNSUPPORTED_FILE = "File type is not supported (yet).";
		public static final String DELETE_FILE = "Delete button clicked.";
	}
	private final int defaultFrameWidth = 1200;
	private final int defaultFrameHeight = 800;
	
	private String filePath;
	private String statusMessage;
	private Color backgroundColor = new Color(255, 235, 255);
	
	public Dimension getFrameDesfaultSize() {
		return new Dimension(defaultFrameWidth, defaultFrameHeight);
	}
	
	public String getfilePath() {
		return filePath;
	}
	public void setfilePath(String currentFilePath) {
		this.filePath = currentFilePath;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

}
