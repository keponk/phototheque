package upsud.hci909.lab3;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.io.File;
import java.util.List;

import javax.swing.JComponent;

@SuppressWarnings("serial")
public class PComponent extends JComponent{
	
	private PComponentModel model;
	private PComponentView view;
	private boolean isFlipped = false;
	private Annotation currentSelection;
	private PController.State state;
	private PView pviewParent;

	public PComponent(PView parent, String filePath) {
		pviewParent = parent;
		model = new PComponentModel(filePath);
		view = new PComponentView(this);
		model.addChangeListener(e -> repaint());
		view.reloadImage();
		state = PController.State.DRAW;
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		view.paintComponent((Graphics2D)g);
	}
	
	public void flip() {
		isFlipped = !isFlipped;
		repaint();
	}
	
	public File getFile() {
		return model.getFile();
	}
	
	public boolean isFlipped() {
		return isFlipped;
	}
	
	public List<Annotation> getAnnotations(){
		return model.getAnnotations();
	}
	
	public void add(Annotation annotation) {
		model.add(annotation);
	}

	public Annotation returnSelection(Point point) {
		for (Annotation annotation : model.getAnnotations()) {
			if ( annotation.hasPoint(point) ) {
				return annotation;
			}
		}
		return null;  // I know, I'm sorry. Not sure how to do this cleanly.
	}
	
	public void setState(PController.State newState) {
		state = newState;
		System.out.println("State changed to " + state.toString());
	}
	
	public PController.State getState(){
		return state;
	}
	
	public void setAnnotationSelected(Annotation selectedAnnotation) {
		currentSelection = selectedAnnotation;
		pviewParent.setAnnotationButtons(currentSelection.getType());
	}
	
	public Annotation getCurrentAnnotation() {
		return currentSelection;
	}
	
	public void clearAnnotationSelected() {
		pviewParent.resetAnnotationButtons();
	}
	
	public void toggleBold() {
		((TextAnnotation) currentSelection).toggleBold();
	}
	
	public void toggleUnderline() {
		((TextAnnotation) currentSelection).toggleUnderline();
	}
	
	public void toggleFontFamily() {
		((TextAnnotation) currentSelection).toggleFamily();
	}
	
	public void changeAnnotationSize() {
		currentSelection.changeSize();
	}

}
