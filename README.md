# Phototheque

Advanced Programming of Interactive Systems.
Course at Université Paris-Sud

For the course project, incremental tasks will be given through the course duration to complete a _Photo Viewer_ application in Java.
The project is organized in packages per tasks (labs).
