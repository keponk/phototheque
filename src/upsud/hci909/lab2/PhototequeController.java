package upsud.hci909.lab2;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.EventObject;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JToggleButton;

public class PhototequeController {

	private PhototequeModel ptmodelInstance;
	private PhototequeView ptviewInstace;

	public PhototequeController(PhototequeModel model, PhototequeView view) {
		ptmodelInstance = model;
		ptviewInstace = view;
		initView();
	}

	private void initView() {
		ptviewInstace.getStatusLabel().setText("Phototeque Ready");
	}
	
	public void updateStatusLabelMessage(String message) {
		ptviewInstace.getStatusLabel().setText(message);
		
	}

	public void triggerFileImport(EventObject event) {
		String updateMessage;
		JFileChooser fileChooser = new JFileChooser();
		int returnVal = fileChooser.showOpenDialog((Component) event.getSource());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			String path= file.getAbsolutePath();
			if( path.contains("jpg") || path.contains("jpeg") || path.contains("png")) {
				ptmodelInstance.setfilePath(path);
				ptviewInstace.setPhototequeComponent(ptmodelInstance.getfilePath());
				updateMessage = file.getName() + " was selected.";
			} else {
				updateMessage = PhototequeModel.UpdateMessages.UNSUPPORTED_FILE;
			}
			
		} else {
			updateMessage = "Open command cancelled by user.";
		}
		ptmodelInstance.setStatusMessage(updateMessage);
		updateStatusLabelMessage(ptmodelInstance.getStatusMessage());

	}

	public void triggerHomeButton() {
		ptmodelInstance.setStatusMessage(PhototequeModel.UpdateMessages.HOME_CLICK);
		updateStatusLabelMessage(ptmodelInstance.getStatusMessage());

	}

	public void initController() {

	}

	public void triggerBackButton() {
		ptmodelInstance.setStatusMessage(PhototequeModel.UpdateMessages.BACK_CLICK);
		updateStatusLabelMessage(ptmodelInstance.getStatusMessage());

	}

	public void triggerNextButton() {
		ptmodelInstance.setStatusMessage(PhototequeModel.UpdateMessages.FORWARD_CLICK);
		updateStatusLabelMessage(ptmodelInstance.getStatusMessage());
	}

	public void triggerToggleButton() {
		ptmodelInstance.setStatusMessage(PhototequeModel.UpdateMessages.TOGGLE_CLICK);
		updateStatusLabelMessage(ptmodelInstance.getStatusMessage());
	}

	public void triggerFileDelete(ActionEvent e) {
		if ( ptviewInstace.getPhototequeComponent() != null ) {
			ptviewInstace.removePhototequeComponent();
			ptmodelInstance.setStatusMessage(PhototequeModel.UpdateMessages.DELETE_FILE);
			updateStatusLabelMessage(ptmodelInstance.getStatusMessage());
		}
	}

	public void triggerFileQuit() {
		System.exit(0);
	}

	public void triggerViewPhotoViewer(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void triggerViewBrowser(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void triggerViewSplitMode(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void triggerFontType() {
		// TODO Auto-generated method stub
		//ptviewInstace.getPhototequeComponentModel().setFont(font);
	}

	public void triggerFontBold(ActionEvent e) {
		JToggleButton button = (JToggleButton) e.getSource();
		ptviewInstace.getPhototequeComponentModel().setBold(button.isSelected());
		System.out.println("bOLD: " + button.isSelected());
	}

}
