package upsud.hci909.lab3;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.EventObject;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;

public class PController {

	private PModel model;
	private PView view;
	public enum State { DRAW, EDIT};

	public PController(PModel model, PView view) {
		this.model = model;
		this.view = view;
		initView();
	}

	private void initView() {
		view.getStatusLabel().setText("Phototeque Ready");
	}
	
	public void updateStatusLabelMessage(String message) {
		view.getStatusLabel().setText(message);
		
	}

	public void triggerFileImport(EventObject event) {
		String updateMessage;
		JFileChooser fileChooser = new JFileChooser();
		int returnVal = fileChooser.showOpenDialog((Component) event.getSource());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			String path= file.getAbsolutePath();
			if( path.contains("jpg") || path.contains("jpeg") || path.contains("png")) {
//				model.setfilePath(path);
				view.setComponent(path);
				updateMessage = file.getName() + " was selected.";
			} else {
				updateMessage = PModel.UpdateMessages.UNSUPPORTED_FILE;
			}
		} else {
			updateMessage = "Open command cancelled by user.";
		}
		model.setStatusMessage(updateMessage);
		updateStatusLabelMessage(model.getStatusMessage());

	}

	public void triggerHomeButton() {
		model.setStatusMessage(PModel.UpdateMessages.HOME_CLICK);
		updateStatusLabelMessage(model.getStatusMessage());

	}

	public void initController() {

	}

	public void triggerBackButton() {
		model.setStatusMessage(PModel.UpdateMessages.BACK_CLICK);
		updateStatusLabelMessage(model.getStatusMessage());

	}

	public void triggerNextButton() {
		model.setStatusMessage(PModel.UpdateMessages.FORWARD_CLICK);
		updateStatusLabelMessage(model.getStatusMessage());
	}

	public void triggerToggleButton() {
		model.setStatusMessage(PModel.UpdateMessages.TOGGLE_CLICK);
		updateStatusLabelMessage(model.getStatusMessage());
		System.out.println("toggle select/draw modes");
	}

	public void triggerFileDelete(ActionEvent e) {
		if ( view.getPhototequeComponent() != null ) {
			view.removePhototequeComponent();
			model.setStatusMessage(PModel.UpdateMessages.DELETE_FILE);
			updateStatusLabelMessage(model.getStatusMessage());
		}
	}

	public void triggerFileQuit() {
		System.exit(0);
	}

	public void triggerViewPhotoViewer(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void triggerViewBrowser(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void triggerViewSplitMode(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void triggerFontBold() {
		view.getPhototequeComponent().toggleBold();
		updateStatusLabelMessage("Bold toggled.");
	}
	
	public void triggerFontFamily() {
		view.getPhototequeComponent().toggleFontFamily();
		updateStatusLabelMessage("Font Family changed.");
	}
	
	public void triggerFontUnderline() {
		view.getPhototequeComponent().toggleUnderline();
		updateStatusLabelMessage("Underline toggled.");
	}
	
	public void triggerAnnotationSize() {
		view.getPhototequeComponent().changeAnnotationSize();
		updateStatusLabelMessage("Annotation size updated.");
	}
	
	public void triggerColorChooser(Annotation currentAnnotation) {
		Color newColor = JColorChooser.showDialog(view.getPane(), "Choose color", currentAnnotation.getColor());
		if (newColor != null ) { // I try to not use null-checks but I failed.
			currentAnnotation.setColor(newColor);
		}
		
	}
	
}
