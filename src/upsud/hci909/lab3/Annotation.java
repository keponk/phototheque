package upsud.hci909.lab3;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public abstract class Annotation {
	
	private List<ChangeListener> changeListeners = new ArrayList<>();
	private Color annotationColor = Color.BLACK;
	enum type {
		TEXT,
		STROKE
	}
	
	public void addChangeListener(ChangeListener listener) {
		changeListeners.add(listener);
	}
	
	public boolean removeChangeListener(ChangeListener listener) {
		return changeListeners.remove(listener);
	}
	
	protected void fireChangeListeners() {
		for (ChangeListener listener : changeListeners) {
			listener.stateChanged(new ChangeEvent(this));
		}
	}
	
	public Color getColor() {
		return annotationColor;
	}
	public void setColor(Color color) {
		annotationColor = color;
		fireChangeListeners();
	}
	
	public abstract void changeSize();
	public abstract void updatePosition(Point newPosition);
	public abstract boolean hasPoint(Point point);
	public abstract void draw(Graphics2D g);
	public abstract type getType();
	

}
