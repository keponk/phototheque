package upsud.hci909.lab1;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.border.BevelBorder;

public class MainView implements ActionListener {

	static final private String PREVIOUS = "previous";
	static final private String NEXT = "next";
	JTextArea mainArea;
	JLabel statusLabel;

	public JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		// Menus
		JMenu fileMenu, viewMenu;
		// Items in File menu
		JMenuItem menuItemImport, menuItemDelete, menuItemQuit;
		// Items in View menu
		JMenuItem menuItemPhoto, menuItemBrowser, menuItemSplit;

		// Create `File` menu
		fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.getAccessibleContext().setAccessibleDescription("I'm learning to think about blind people");
		menuBar.add(fileMenu);

		menuItemImport = new JMenuItem("Import", KeyEvent.VK_I);
		menuItemImport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JFileChooser fileChooser = new JFileChooser();
				int returnVal = fileChooser.showOpenDialog((Component) event.getSource());
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					// This is where a real application would open the file.
					statusLabel.setText("Opening: " + file.getName() + ".");
				} else {
					statusLabel.setText("Open command cancelled by user.");
				}
		    }
		});
		fileMenu.add(menuItemImport);

		menuItemDelete = new JMenuItem("Delete", KeyEvent.VK_D);
		menuItemDelete.addActionListener(this);
		fileMenu.add(menuItemDelete);

		menuItemQuit = new JMenuItem("Quit", KeyEvent.VK_Q);
		menuItemQuit.addActionListener(this);
		fileMenu.add(menuItemQuit);

		// Create `View` menu
		viewMenu = new JMenu("View");
		viewMenu.setMnemonic(KeyEvent.VK_V);
		fileMenu.getAccessibleContext().setAccessibleDescription("View menu");
		menuBar.add(viewMenu);

		menuItemPhoto = new JMenuItem("Photo Viewer", KeyEvent.VK_P);
		menuItemPhoto.addActionListener(this);
		viewMenu.add(menuItemPhoto);

		menuItemBrowser = new JMenuItem("Browser", KeyEvent.VK_B);
		menuItemBrowser.addActionListener(this);
		viewMenu.add(menuItemBrowser);

		menuItemSplit = new JMenuItem("Split Mode", KeyEvent.VK_S);
		menuItemSplit.addActionListener(this);
		viewMenu.add(menuItemSplit);

		return menuBar;
	}

	public JButton makeNavigationButton(String imageName, String actionCommand, String toolTipText, String altText) {
		// Look for the image.
		String imgLocation = "/" + imageName + ".png";
		URL imageURL = MainView.class.getResource(imgLocation);

		// Create and initialize the button.
		JButton button = new JButton();
		button.setActionCommand(actionCommand);
		button.setToolTipText(toolTipText);
		button.addActionListener(this);

		if (imageURL != null) { // image found
			button.setIcon(new ImageIcon(imageURL, altText));
		} else { // no image found
			button.setText(altText);
			System.err.println("Resource not found: " + imgLocation);
		}

		return button;
	}

	public Container createContentPane() {
		JToolBar mainToolBar;
		JScrollPane scrollPane;

		// Create the content-pane-to-be.
		JPanel contentPane = new JPanel(new BorderLayout());
		contentPane.setOpaque(true);

		// Create the ToolBar
		mainToolBar = new JToolBar("Toolbar");
		addButtons(mainToolBar);
		mainToolBar.setFloatable(false);
		mainToolBar.setRollover(true);
		contentPane.add(mainToolBar, BorderLayout.NORTH);

		// Create a scrolled text area.
		mainArea = new JTextArea(5, 30);
		mainArea.setWrapStyleWord(true);
		mainArea.setLineWrap(true);
		mainArea.setEditable(false);
		scrollPane = new JScrollPane(mainArea);

		// Add the text area to the content pane.
		contentPane.add(scrollPane, BorderLayout.CENTER);

		statusLabel = new JLabel("Phototeque ready.");
		statusLabel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		contentPane.add(statusLabel, BorderLayout.SOUTH);

		return contentPane;
	}

	private void addButtons(JToolBar mainToolBar) {
		JButton button = null;

		// first button
		button = makeNavigationButton("Home24", "home", "Back to main page", "Home");
		mainToolBar.add(button);
		
		// second button
		button = makeNavigationButton("Back24", PREVIOUS, "Back to previous something-or-other", "Previous");
		mainToolBar.add(button);

		// third button
		button = makeNavigationButton("Forward24", NEXT, "Forward to something-or-other", "Next");
		mainToolBar.add(button);

		// separator
		mainToolBar.addSeparator();

		// toggle button
		
		JToggleButton toggleButton = new JToggleButton();

		String imgDefaultIconLocation = "/vacation24.png";
		URL imgDefaultIconURL = MainView.class.getResource(imgDefaultIconLocation);
		if (imgDefaultIconURL != null) {                      //image found
			toggleButton.setIcon(new ImageIcon(imgDefaultIconURL, "Vacation"));
        } else {                                     //no image found
        	toggleButton.setText("Vacation");
            System.err.println("Resource not found: "
                               + imgDefaultIconLocation);
        }
		String imgSelectedIconLocation = "/family24.png";
		URL imgSelectedIconURL = MainView.class.getResource(imgSelectedIconLocation);
		if (imgSelectedIconURL != null) {                      //image found
			toggleButton.setSelectedIcon(new ImageIcon(imgSelectedIconURL, "Vacation"));
        } else {                                     //no image found
        	toggleButton.setText("Vacation");
            System.err.println("Resource not found: "
                               + imgSelectedIconLocation);
        }
		toggleButton.setToolTipText("Vacation");
		toggleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event ) {
				JToggleButton jtb = (JToggleButton) event.getSource();
				if (jtb.isSelected()) {
					statusLabel.setText("Family selected");
				} else {
					statusLabel.setText("Vacation selected.");
				}
		    }
		});
		mainToolBar.add(toggleButton);

	}

	private static void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("Phototeque_Lab1");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		MainView thisLab = new MainView();
		frame.setJMenuBar(thisLab.createMenuBar());
		frame.setContentPane(thisLab.createContentPane());

		// Display the window.
		frame.setSize(800, 600);
		frame.setVisible(true);

	}

	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String statusUpdate = "General action event detected.";
		statusLabel.setText(statusUpdate);
		mainArea.append(statusUpdate + "\n");
		mainArea.append(e.getSource().toString() + "\n");
		mainArea.setCaretPosition(mainArea.getDocument().getLength());

	}

}
