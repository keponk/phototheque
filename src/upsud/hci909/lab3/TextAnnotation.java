package upsud.hci909.lab3;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TextAnnotation extends Annotation {

	private Point position;
	private Point topLeftPos;
	private Point bottomRightPos;
	private Point dragPosition;
	private StringBuffer text = new StringBuffer();
	private ArrayList<Integer> lineBreaks;
	private ArrayList<String> lines;
	private boolean lineBreaksValid;
	private int plainOrBold = Font.PLAIN; // i hate this name
	private int fontSize = 14;
	private boolean isUnderlined = false;
	private String currentFontFamily = Font.SANS_SERIF;
	private Font font;
	private Map<TextAttribute, Integer> fontAttributes;

	public TextAnnotation(Point point) {
		position = point;
		bottomRightPos = position;
		font = new Font(currentFontFamily, plainOrBold, fontSize);
		fontAttributes = new HashMap<TextAttribute, Integer>();
	}

	public type getType() {
		return type.TEXT;
	}

	public void add(char keyChar) {

		switch (keyChar) {
		case KeyEvent.VK_BACK_SPACE:
			text.deleteCharAt(text.length() - 1);
			break;
		case KeyEvent.VK_ENTER:
			System.out.println("supposedly aded new line");
			text.append(System.lineSeparator());
//			text.append(' ');
		default:
			text.append(keyChar);
		}
		invalidateLineBreaks();
		fireChangeListeners();
	}

	public void add(String newText) {
		text.append(newText);
		invalidateLineBreaks();
		fireChangeListeners();
	}

	private void invalidateLineBreaks() {
		lineBreaksValid = false;
	}
	
	private void updateFont() {
		font = new Font(currentFontFamily, plainOrBold, fontSize);
		if (isUnderlined) {
			fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		} else {
			fontAttributes.put(TextAttribute.UNDERLINE, -1);
		}
		font = font.deriveFont(fontAttributes);
		invalidateLineBreaks();
		fireChangeListeners();
	}

	public void toggleBold() {
		plainOrBold = font.getStyle() == Font.BOLD ? Font.PLAIN : Font.BOLD;
		updateFont();		
	}

	public void toggleFamily() {
		if (currentFontFamily.equals(Font.SANS_SERIF)) {
			currentFontFamily = Font.MONOSPACED;
		} else if (currentFontFamily.equalsIgnoreCase(Font.MONOSPACED)) {
			currentFontFamily = Font.SERIF;
		} else {
			currentFontFamily = Font.SANS_SERIF;
		}
		updateFont();
	}

	public void toggleUnderline() {
		isUnderlined = isUnderlined ? false : true;
		updateFont();
	}

	public void updatePosition(Point newPosition) {
		int xDelta = dragPosition.x - newPosition.x;
		int yDelta = dragPosition.y - newPosition.y;
		position.x -= xDelta;
		position.y -= yDelta;
		dragPosition = newPosition;
		fireChangeListeners();
	}

	public boolean hasPoint(Point point) {
		if (text.length() > 0) {
			if (point.x > topLeftPos.x && point.x < bottomRightPos.x && point.y > topLeftPos.y
					&& point.y < bottomRightPos.y) {
				dragPosition = point;
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void changeSize() {
		fontSize = fontSize > 22 ? 14 : fontSize + 4;
		updateFont();
	}

	@Override
	public void draw(Graphics2D g) {
		FontMetrics fontMetrics = g.getFontMetrics();
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g.setColor(getColor());
		g.setFont(font);
		Point lineStart = new Point(position);
		int longestLineSize = 0;

		if (!lineBreaksValid) {
			wrapLines(g);
		}

		for (String line : lines) {
			g.drawString(line, lineStart.x, lineStart.y);
			lineStart.y += fontMetrics.getHeight();
			longestLineSize = fontMetrics.stringWidth(line) > longestLineSize ? fontMetrics.stringWidth(line)
					: longestLineSize;
		}
		topLeftPos = new Point(position.x, position.y - fontMetrics.getHeight());
		bottomRightPos = new Point(position.x + longestLineSize, lineStart.y - fontMetrics.getHeight());
	}

	private ArrayList<String> wrapLines(Graphics2D g) {
		lines = new ArrayList<>();
		String string = text.toString();
		lineBreaks = calculateLineBreaks(g);

		int lineStart = 0;
		for (int breakPoint : lineBreaks) {
			lines.add(string.substring(lineStart, breakPoint));
			lineStart = breakPoint;
		}

		// LAST LINE
		if (lineStart < string.length()) {
			lines.add(string.substring(lineStart));
		}
		return lines;
	}

	private ArrayList<Integer> calculateLineBreaks(Graphics2D g) {
		FontMetrics fontMetrics = g.getFontMetrics();
		int rightBound = g.getClip().getBounds().width;
		int availableWidth = rightBound - position.x;
		String string = text.toString();
		ArrayList<Integer> breaks = new ArrayList<>();

		int lineStart = 0;
		int lastSpace = 0;
		int lastNewLine = 0;

		for (int i = 0; i < string.length(); i++) {
			char ch = string.charAt(i);
			int lineWidth = fontMetrics.stringWidth(string.substring(lineStart, i + 1));
			if (Character.isWhitespace(ch) || ch == Character.LINE_SEPARATOR) {
				lastSpace = Character.isWhitespace(ch) ? i : lastSpace;
				lastNewLine = ch == Character.LINE_SEPARATOR ? i : lastNewLine;
				continue;
			}
			if (lineWidth > availableWidth || lastNewLine == i) {
				int breakPoint = 0;
				if (lastNewLine > lineStart) {
					breakPoint = lastNewLine + 1;
					lastNewLine = breakPoint;
				} else if (lastSpace > lineStart) {
					breakPoint = lastSpace + 1;
					lastSpace = breakPoint;
				} else {
					breakPoint = i;
				}
//				int breakPoint = lastSpace > lineStart ? lastSpace + 1 : i;
				breaks.add(breakPoint);
				lineStart = breakPoint;
//				lastSpace = breakPoint;
			}
		}
		return lineBreaks = breaks;
	}

}
