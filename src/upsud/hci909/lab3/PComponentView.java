package upsud.hci909.lab3;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;

import javax.swing.ImageIcon;

public class PComponentView implements MouseListener, MouseMotionListener, KeyListener {

	private PComponent controller;
	private Image image;
	private int counter = 0;
	private Stroke currentStroke;
	private TextAnnotation currentTextAnnotation;
	private Point dragStart;
	private Annotation modifyAnnotation;

	public PComponentView(PComponent component) {
		controller = component;
		controller.addMouseListener(this);
		controller.addMouseMotionListener(this);
		controller.addKeyListener(this);
		controller.setFocusable(true);
	}

	public void reloadImage() {
		File file = controller.getFile();
		image = new ImageIcon(file.getAbsolutePath()).getImage();
		System.out.println("Size: " + image.getWidth(controller) + " " + image.getHeight(controller));
		controller.setSize(new Dimension(image.getWidth(null), image.getHeight(null)));
		controller.setPreferredSize(new Dimension(image.getWidth(controller), image.getHeight(controller)));
//		controller.setBorder(new LineBorder(Color.WHITE, 10));
	}

	public void paintComponent(Graphics2D g) {
		System.out.println(counter + " painting");
//		System.out.println("Size: " + controller.getSize());
		g.setClip(0, 0, controller.getPreferredSize().width, controller.getPreferredSize().height);
		counter++;
		drawImage(g);
		if (controller.isFlipped()) {
			drawAnnotations(g);
		}

	}

	private void drawImage(Graphics2D g) {
		g.drawImage(image, 0, 0, controller);
	}

	private void drawAnnotations(Graphics2D g) {
		for (Annotation annotation : controller.getAnnotations()) {
			annotation.draw(g);
		}
	}
	
	private void setSelectedAnnotation(Point p) {
		Annotation annotation = controller.returnSelection(p);
		if (annotation != null) {
			System.out.println("annotation found!");
			modifyAnnotation = annotation;	
			controller.setAnnotationSelected(modifyAnnotation);
		} else {
			modifyAnnotation = null;
			controller.clearAnnotationSelected();
		}
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if (controller.getState() == PController.State.DRAW) {
			if (currentStroke == null) {
				return;
			}
			currentStroke.add(e.getPoint());
		} else {
			//setSelectedAnnotation(e.getPoint());
			if (modifyAnnotation != null) {
				modifyAnnotation.updatePosition(e.getPoint());
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
			controller.flip();
		} else {
			if (controller.isFlipped() && controller.getState() == PController.State.DRAW) {
				currentTextAnnotation = new TextAnnotation(e.getPoint());
				controller.add(currentTextAnnotation);
				controller.requestFocusInWindow();
			} else {
				setSelectedAnnotation(e.getPoint());
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
//		System.out.println("PRESSED");
		if (!controller.isFlipped()) {
			return;
		}
		if (controller.getState() == PController.State.DRAW) {
			if (currentTextAnnotation != null) {
				currentTextAnnotation = null;
			}
			dragStart = e.getPoint();
			currentStroke = new Stroke(dragStart);
			controller.add(currentStroke);
		} else {
			// I suspect this can be empty
			setSelectedAnnotation(e.getPoint());
		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (controller.getState() == PController.State.DRAW) {
			currentStroke = null;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (controller.getState() == PController.State.DRAW) {
			if (currentTextAnnotation == null) {
				System.out.println("currentTextAnnotation is null");
				return;
			}
			currentTextAnnotation.add(e.getKeyChar());
		} else {
			// TODO - probably do nothing
		}
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

}
