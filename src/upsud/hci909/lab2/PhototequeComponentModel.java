package upsud.hci909.lab2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;

public class PhototequeComponentModel {
	public final Color BORDER_COLOR = new Color(255, 255, 179);
	public final int BORDER_SIZE = 10;
	public final Color CANVAS_COLOR = Color.WHITE;
	public final Color STROKE_COLOR = Color.BLACK;
	public final Color TEXT_COLOR = Color.BLACK;
	public final int FONT_SIZE = 12;
	public final Font FONT = new Font(Font.SANS_SERIF, Font.BOLD, FONT_SIZE);
	public final BasicStroke PHOTOTEQUE_STROKE = new BasicStroke(5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	
	private float brushSize;
	private BasicStroke stroke;
	private Color strokeColor;
	private Font font;
	private String filePath;
	private boolean underline;
	private boolean bold;
	private String[] fontTypes = {Font.SANS_SERIF, Font.SERIF, Font.MONOSPACED};
	
	public PhototequeComponentModel(String filePath) {
		this.filePath = filePath;
		stroke = PHOTOTEQUE_STROKE;
		font = FONT;
		underline = false;
		bold = false;
		brushSize = stroke.getLineWidth();
		strokeColor = STROKE_COLOR;
		
	}
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public float getBrushSize() {
		return brushSize;
	}

	public BasicStroke getStroke() {
		return stroke;
	}

	public Font getFont() {
		return font;
	}

	public boolean isUnderline() {
		return underline;
	}

	public boolean isBold() {
		return bold;
	}

	public void setBrushSize(float brushSize) {
		this.brushSize = brushSize;
	}

	public void setStroke(BasicStroke stroke) {
		this.stroke = stroke;
	}

	public void changeFont() {
		// DO SOMETHING}
	}

	public void setUnderline(boolean underline) {
		this.underline = underline;
	}

	public void setBold(boolean bold) {
		this.bold = bold;
		if (bold) {
			font = new Font(font.getFamily(), Font.BOLD, font.getSize());
		} else {
			font = new Font(font.getFamily(), Font.PLAIN, font.getSize());
		}
		
	}

	public Color getStrokeColor() {
		return strokeColor;
	}

	public void setStrokeColor(Color strokeColor) {
		this.strokeColor = strokeColor;
	}
}
