package upsud.hci909.lab3;

public class Phototeque {
	
	public static void main(String[] args) {
		PModel model = new PModel();
		PView view = new PView(model);
		PController controller = new PController(model, view);
		view.setController(controller);
	}

}
