package upsud.hci909.lab3;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

public class Stroke extends Annotation {

	private BasicStroke strokeDecor = new BasicStroke(5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	private List<Point> points = new ArrayList<Point>();
	private float strokeWidth = 5f;
	private Point dragPosition;

	public Stroke(Point start) {
		points.add(start);
	}
	
	public type getType() {
		return type.STROKE;
	}

	public void add(Point point) {
		points.add(point);
		fireChangeListeners();
	}

//	public void setStrokeWidth(int width) {
//		strokeWidth = width;
//	}
	
	public void updatePosition(Point newPosition ) {
		int xDelta = dragPosition.x - newPosition.x;
		int yDelta = dragPosition.y - newPosition.y;
		for (Point point : points) {
			point.x -= xDelta;
			point.y -= yDelta;
		}
		dragPosition = newPosition;
		fireChangeListeners();
	}

	public boolean hasPoint(Point point) {
		float errorRate = strokeWidth/2;
		Point prevPoint = points.get(0);
		for (Point p : points.subList(1, points.size())) {
			Line2D line = new Line2D.Float((float) prevPoint.x, (float) prevPoint.y, (float) p.x, (float) p.y);
			float dist = (float)line.ptSegDist(point);
			if (dist < errorRate) {
				System.out.println("Stroke found!");
				dragPosition = point;
				return true;
			}
			prevPoint = p;
		}
		return false;
	}
	
	@Override
	public void changeSize() {
		strokeWidth = strokeWidth > 11 ? 5 : strokeWidth + 3;
		strokeDecor = new BasicStroke(strokeWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		fireChangeListeners();
	}

	@Override
	public void draw(Graphics2D g) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		        RenderingHints.VALUE_ANTIALIAS_ON);
		if (points.size() < 2) {
			return;
		}
		g.setColor(this.getColor());
		g.setStroke(strokeDecor);
		Point prevPoint = points.get(0);
		for (Point nextPoint : points.subList(1, points.size())) {
			g.drawLine(prevPoint.x, prevPoint.y, nextPoint.x, nextPoint.y);
			prevPoint = nextPoint;
		}
	}
}
