package upsud.hci909.lab2;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class PhototequeComponent extends JComponent {

	private int counter = 0;
	private boolean flipped;
	private boolean outOfBounds;
	private String imageFileAbolutePath;
	private BufferedImage image;
	private Dimension imageDimension;
	private JLabel photoArea;
	private Point cursorLocation;
	private ArrayList<Point> currentStroke;
	private ArrayList<ArrayList<Point>> strokesList;
	private String currentText;
	private ArrayList<PhototequeTextEntry> textList;
	private PhototequeComponentModel ptComponentModel;
	private boolean drawing;
//	private float currentStrokeSize;
//	private int currentFont;

	public PhototequeComponent(PhototequeComponentModel ptComponentModel) {
		this.ptComponentModel = ptComponentModel;
		imageFileAbolutePath = ptComponentModel.getFilePath();
		try {
			System.out.println(imageFileAbolutePath);
			image = ImageIO.read(new File(imageFileAbolutePath));
			System.out.println("Image set");
		} catch (IOException ex) {
			System.err.println("Something went wrong loading the image");
		}
		imageDimension = new Dimension(image.getWidth(), image.getHeight());
		cursorLocation = new Point();
		
		
		currentStroke = new ArrayList<Point>();
		strokesList = new ArrayList<ArrayList<Point>>();
		strokesList.add(currentStroke);
		
		textList = new ArrayList<PhototequeTextEntry>();
		currentText = "";

		setupPhotoArea();
		setupAnnotationArea();
		setComponentsSizes();

		flipped = false;
		drawing = false;
		outOfBounds = false;
		setComponentsVisibility();

		add(photoArea);
		setVisible(true);
		setOpaque(true);
		validate();
	}

	private void setupPhotoArea() {
		photoArea = new JLabel();
		photoArea.setBorder(new LineBorder(ptComponentModel.BORDER_COLOR, ptComponentModel.BORDER_SIZE));
		photoArea.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
					toggleComponentArea();
				}
			}
		});
		photoArea.setIcon(new ImageIcon(image));
		photoArea.setVisible(true);
	}

	private void setupAnnotationArea() {
		setBorder(new LineBorder(ptComponentModel.BORDER_COLOR, ptComponentModel.BORDER_SIZE));
		addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				triggerKeyTyped(e.getKeyChar());
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
					toggleComponentArea();
				} else {
					triggerMouseClicked(e.getPoint());
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				triggerMouseReleased(e);
			}
		});
		addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				triggerMouseDragged(e);
			}
		});
		setFocusable(true);
		setVisible(false);
	}

	private void triggerMouseClicked(Point mouseLocation) {
		if (currentText.length() > 0) {
			textList.add(new PhototequeTextEntry(cursorLocation, currentText));
		}
		cursorLocation = mouseLocation;
		currentText = "";
		grabFocus();
	}

	private void triggerKeyTyped(char keyChar) {
		System.out.println("KEY TYPED: " + keyChar);
		Point nextLine = new Point(cursorLocation.x, cursorLocation.y + 16);
		switch (keyChar) {
		case KeyEvent.VK_BACK_SPACE:
			if (currentText.length() > 0) {
				currentText = currentText.substring(0, currentText.length() - 1);
			}
			break;
		case KeyEvent.VK_ENTER:
			triggerMouseClicked(nextLine);
			break;
		default:
			int textSize = currentText.length() * ptComponentModel.getFont().getSize() + 1;
			if ((int) textSize + cursorLocation.x > getWidth()) {
				int lastWhiteSpace = currentText.lastIndexOf(" ");
				String textToKeep = "";
				if (lastWhiteSpace != -1) {
					textToKeep = currentText.substring(lastWhiteSpace + 1);
					currentText = currentText.substring(0, lastWhiteSpace);
				}
				triggerMouseClicked(nextLine);
				currentText = textToKeep;
			}
			if (cursorLocation.y < getHeight() - (ptComponentModel.getFont().getSize() + 3)) {
				currentText += keyChar;
			}
			break;
		}
		System.out.println("text so far:" + currentText);
		repaint();
	}

	private void triggerMouseDragged(MouseEvent e) {
		Point location = e.getPoint();
		if ( location.x < 0 || location.x > getWidth() || location.y < 0 || location.y > getHeight() ) {
			outOfBounds = true;
			triggerMouseReleased(e);
			drawing = false;
			// CHEC IF OUTSIDE BOUNDS
			// IF SO FINISHSTROKE
			// NEED TO FIGURE OUT WHAT TO DO WHEN RE-ENTERING
		} else {
			currentStroke.add(e.getPoint());
			drawing = true;
		}
//		currentStroke.add(e.getPoint());
//		drawing = true;
		repaint();
		
		
	}

	private void triggerMouseReleased(MouseEvent e) {
		if (drawing) {
			if (!outOfBounds) {
				currentStroke.add(e.getPoint());
				outOfBounds = false;
			}
			currentStroke = new ArrayList<Point>();
			strokesList.add(currentStroke);
			drawing = false;
		}
		repaint();
	}

	private void toggleComponentArea() {
		System.out.println("double clicked");
		flipped = !flipped;
		setComponentsVisibility();
	}

	private void setComponentsSizes() {
		setSize(imageDimension);
		setPreferredSize(imageDimension);
		photoArea.setSize(getSize());
		photoArea.setPreferredSize(getPreferredSize());
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		setComponentsSizes();
		counter++;
		System.out.println(counter + " repaints");
		if (flipped) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setColor(ptComponentModel.CANVAS_COLOR);
			g2.fillRect(0, 0, getWidth(), getHeight());
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			paintStrokes(g2);
			paintStrings(g2);
		}
	}

	private void setComponentsVisibility() {
		photoArea.setVisible(!flipped);
		setFocusable(flipped);
		if (flipped) {
			grabFocus();
		}
	}

	private void paintStrings(Graphics2D g2) {
		
		g2.setFont(ptComponentModel.getFont());
		//g2.setColor(Color.BLACK);
		if (!textList.isEmpty()) {
			for (PhototequeTextEntry entry : textList) {
				int x = entry.getPoint().x;
				int y = entry.getPoint().y;
				String text = entry.getText();
				g2.drawString(text, x, y);
			}
		}

		if (currentText.length() > 0) {
			g2.drawString(currentText, cursorLocation.x, cursorLocation.y);
		}
	}

//	private void paintTest(Graphics 2D) {
//		// TEST CODE
//		Random rnd = new Random();
//		int r = rnd.nextInt(256);
//		int g = rnd.nextInt(256);
//		int b = rnd.nextInt(256);
//		int x = rnd.nextInt(getWidth());
//		int y = rnd.nextInt(getHeight());
//		int w = rnd.nextInt(256);
//		int h = rnd.nextInt(256);
//		Color randColor = new Color(r, g, b);
//		g2.setColor(randColor);
//		g2.fillOval(x, y, 10, 20);
//	}

	private void paintStrokes(Graphics2D g2) {
		g2.setColor(ptComponentModel.getStrokeColor());
		g2.setStroke(ptComponentModel.getStroke());
		for (ArrayList<Point> pointarraylist : strokesList) {
			if (!pointarraylist.isEmpty()) {
				Point prevPoint = pointarraylist.get(0);
				for (Point p : pointarraylist) {
					g2.drawLine(prevPoint.x, prevPoint.y, p.x, p.y);
					prevPoint = p;
				}
			}
		}
	}

	public BufferedImage getImage() {
		return image;
	}

	public boolean isFlipped() {
		return flipped;
	}

	public void setFlipped(boolean flipped) {
		this.flipped = flipped;
	}

}
