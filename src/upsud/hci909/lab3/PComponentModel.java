package upsud.hci909.lab3;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class PComponentModel {
	
	private String absolutePath;
	private List<Annotation> annotations = new ArrayList<>();
	private File file;
	
	private List<ChangeListener> changeListeners = new ArrayList<>();
	
	public PComponentModel(String filePath) {
		file = new File(filePath);
	}

	public String getAbsolutePath() {
		return absolutePath;
	}
	
	public File getFile(){
		return file;
	}
	
	public void add(Annotation annotation) {
		annotation.addChangeListener(e -> fireChangeListeners());
		annotations.add(annotation);
		fireChangeListeners();
	}
	
	public List<Annotation> getAnnotations() {
		return Collections.unmodifiableList(annotations);
	}
	
	public void addChangeListener(ChangeListener listener) {
		changeListeners.add(listener);
	}
	
	public boolean removeChangeListener(ChangeListener listener) {
		return changeListeners.remove(listener);
	}

	private void fireChangeListeners() {
		for (ChangeListener listener : changeListeners) {
			listener.stateChanged(new ChangeEvent(this));
		}
	}

}
