package upsud.hci909.lab3;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.net.URL;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.border.BevelBorder;

public class PView {

	private JFrame phototequeFrame;
	private JMenuBar menubar;
	private JMenu fileMenu;
	private JMenuItem menuItemImport;
	private JToolBar toolbar;
	private PController controller;
	private JPanel topLevelPanel;
	private JLabel statusLabel;
	private JMenuItem menuItemDelete;
	private JMenuItem menuItemQuit;
	private JMenu viewMenu;
	private JMenuItem menuItemPhoto;
	private JMenuItem menuItemBrowser;
	private JMenuItem menuItemSplit;
//	private PhototequeComponentModel ptComponentModel;
	private PComponent component;
	private JPanel centralPanel;
	private JScrollPane scrollPane;
	private JToggleButton cursorToggleButton;
	private JToggleButton handToggleButton;
//	private JToggleButton boldToggleButton;
	private JButton fontToggleButton;
	private JButton boldToggleButton;
	private JButton underlineToggleButton;
	private JButton colorChooser;
	private JButton brushButton;
	private Box componentBox;
	private final String resourcesPath = "/upsud/hci909/resources/";

	public PView(PModel model) {

		phototequeFrame = new JFrame(model.TITLE);
		phototequeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		phototequeFrame.setSize(model.getFrameDesfaultSize());

		// Create Menu bar
		menubar = new JMenuBar();
		populateMenuBar(menubar);
		phototequeFrame.setJMenuBar(menubar);

		// Create Toolbar
		toolbar = new JToolBar();
		toolbar.setFloatable(false);
		populateToolBar(toolbar);
//		updateEditButtonsStates(false);

		// Main JPanel to host the custom JComponent
		centralPanel = new JPanel();
		centralPanel.setLayout(new BoxLayout(centralPanel, BoxLayout.Y_AXIS));
		centralPanel.setBackground(model.getBackgroundColor());

		scrollPane = new JScrollPane(centralPanel);
		scrollPane.setVisible(true);

		// Create status bar
		statusLabel = new JLabel("Loading...");
		statusLabel.setBorder(new BevelBorder(BevelBorder.LOWERED));

		topLevelPanel = new JPanel(new BorderLayout());
		topLevelPanel.setOpaque(true);
		topLevelPanel.add(toolbar, BorderLayout.NORTH);
		topLevelPanel.add(scrollPane, BorderLayout.CENTER);
		topLevelPanel.add(statusLabel, BorderLayout.SOUTH);

		phototequeFrame.add(topLevelPanel);
		phototequeFrame.setVisible(true);
	}

	private void populateToolBar(JToolBar tb) {
		JButton button = null;

		// first button
		button = makeNavigationButton("home24", "home", "Back to main page", "Home");
		button.addActionListener(e -> {
			controller.triggerHomeButton();
		});
		tb.add(button);

		// second button
		button = makeNavigationButton("back24", "previous", "Back to previous something-or-other", "Previous");
		button.addActionListener(e_ -> {
			controller.triggerBackButton();
		});
		tb.add(button);

		// third button
		button = makeNavigationButton("forward24", "next", "Forward to something-or-other", "Next");
		button.addActionListener(e_ -> {
			controller.triggerNextButton();
		});
		tb.add(button);

		tb.addSeparator();

		JToggleButton toggleButton = new JToggleButton();
		// Front side
		String imgDefaultIconLocation = resourcesPath + "/vacation24.png";
		URL imgDefaultIconURL = getClass().getResource(imgDefaultIconLocation);
		if (imgDefaultIconURL != null) {
			toggleButton.setIcon(new ImageIcon(imgDefaultIconURL, "Vacation"));
		} else {
			toggleButton.setText("Vacation");
			System.err.println("Resource not found: " + imgDefaultIconLocation);
		}
		// Back side
		String imgSelectedIconLocation = resourcesPath + "/family24.png";
		URL imgSelectedIconURL = getClass().getResource(imgSelectedIconLocation);
		if (imgSelectedIconURL != null) {
			toggleButton.setSelectedIcon(new ImageIcon(imgSelectedIconURL, "Family"));
		} else {
			toggleButton.setText("Family");
			System.err.println("Resource not found: " + imgSelectedIconLocation);
		}
		toggleButton.setToolTipText("Vacation");
		toggleButton.addActionListener(e -> {
			controller.triggerToggleButton();
		});
		tb.add(toggleButton);
		
		tb.addSeparator();
		
		
		// ADD SELECT/EDIT BUTTONS
		cursorToggleButton = new JToggleButton();
		String cursorIconPath = resourcesPath + "draw24.png";
		URL cursorIconURL = getClass().getResource(cursorIconPath);
		cursorToggleButton.setIcon(new ImageIcon(cursorIconURL));
		cursorToggleButton.setToolTipText("Select");
		cursorToggleButton.addActionListener( e -> {
			cursorToggleButton.setSelected(true);
			handToggleButton.setSelected(false);
			component.setState(PController.State.DRAW);
		});
		cursorToggleButton.setSelected(true);
		cursorToggleButton.setEnabled(false);
		tb.add(cursorToggleButton);
		
		handToggleButton = new JToggleButton();
		String handIconPath = resourcesPath + "edit24.png";
		URL handIconURL = getClass().getResource(handIconPath);
		handToggleButton.setIcon(new ImageIcon(handIconURL));
		handToggleButton.setToolTipText("Select");
		handToggleButton.addActionListener( e-> {
			handToggleButton.setSelected(true);
			cursorToggleButton.setSelected(false);
			component.setState(PController.State.EDIT);
		});
		handToggleButton.setEnabled(false);
		tb.add(handToggleButton);
		
		tb.addSeparator();
		
		fontToggleButton = makeNavigationButton("font24", "change font", "Set desired font", "Font Type");
		fontToggleButton.addActionListener(e -> {
			controller.triggerFontFamily();
		});
		fontToggleButton.setEnabled(false);
		tb.add(fontToggleButton);
		
//		boldToggleButton = new JToggleButton();
//		String boldButtonIconLocation = resourcesPath + "font_bold24.png";
//		URL boldButtonIconURL = getClass().getResource(boldButtonIconLocation);
//		if (imgDefaultIconURL != null) {
//			boldToggleButton.setIcon(new ImageIcon(boldButtonIconURL));
//		} else {
//			boldToggleButton.setText("Bold");
//			System.err.println("Resource not found: " + imgDefaultIconLocation);
//		}
//		boldToggleButton.setToolTipText("Bold");
//		boldToggleButton.addActionListener( e -> {
//			controller.triggerFontBold(e);
//			
//		});
//		tb.add(boldToggleButton);
		boldToggleButton = makeNavigationButton("font_bold24", "Bold", "Bold", "Bold");
		boldToggleButton.addActionListener( e -> {
			controller.triggerFontBold();
		});
		boldToggleButton.setEnabled(false);
		tb.add(boldToggleButton);
		
		underlineToggleButton = makeNavigationButton("font_underline24", "Underline", "Underline", "Underline");
		underlineToggleButton.addActionListener( e -> {
			controller.triggerFontUnderline();
		});
		underlineToggleButton.setEnabled(false);
		tb.add(underlineToggleButton);
		
		tb.addSeparator();
		
		colorChooser = makeNavigationButton("color24", "Color", "Choose annotation color", "Color");
		colorChooser.addActionListener( e -> {
			controller.triggerColorChooser(component.getCurrentAnnotation());
		});
		colorChooser.setEnabled(false);
		tb.add(colorChooser);
		
		brushButton = makeNavigationButton("size24", "Annotation size", "Change annotation size", "Annotation Size");
		brushButton.addActionListener( e -> {
			controller.triggerAnnotationSize();
		});
		brushButton.setEnabled(false);
		tb.add(brushButton);		
	}

	private JButton makeNavigationButton(String imageFileName, String actionCommand, String toolTipText,
			String altText) {

		String imgLocation =  resourcesPath + String.format("%s.png", imageFileName);
		URL imageURL =  PView.class.getResource(imgLocation);

		// Create and initialize the button.
		JButton button = new JButton();
		button.setActionCommand(actionCommand);
		button.setToolTipText(toolTipText);

		if (imageURL != null) {
			button.setIcon(new ImageIcon(imageURL, altText));
		} else {
			button.setText(altText);
			System.err.println("Resource not found: " + imgLocation);
		}

		return button;
	}

	private void populateMenuBar(JMenuBar mb) {
		fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menubar.add(fileMenu);

		menuItemImport = new JMenuItem("Import", KeyEvent.VK_I);
		menuItemImport.addActionListener(e -> {
			controller.triggerFileImport(e);
		});
		fileMenu.add(menuItemImport);

		menuItemDelete = new JMenuItem("Delete", KeyEvent.VK_D);
		menuItemDelete.addActionListener(e -> {
			controller.triggerFileDelete(e);
		});
		fileMenu.add(menuItemDelete);

		menuItemQuit = new JMenuItem("Quit", KeyEvent.VK_Q);
		menuItemQuit.addActionListener(e -> {
			controller.triggerFileQuit();
		});
		fileMenu.add(menuItemQuit);

		// Create `View` menu
		viewMenu = new JMenu("View");
		viewMenu.setMnemonic(KeyEvent.VK_V);
		fileMenu.getAccessibleContext().setAccessibleDescription("View menu");
		menubar.add(viewMenu);

		menuItemPhoto = new JMenuItem("Photo Viewer", KeyEvent.VK_P);
		menuItemPhoto.addActionListener(e -> {
			controller.triggerViewPhotoViewer(e);
		});
		viewMenu.add(menuItemPhoto);

		menuItemBrowser = new JMenuItem("Browser", KeyEvent.VK_B);
		menuItemBrowser.addActionListener(e -> {
			controller.triggerViewBrowser(e);
		});
		viewMenu.add(menuItemBrowser);

		menuItemSplit = new JMenuItem("Split Mode", KeyEvent.VK_S);
		menuItemSplit.addActionListener(e -> {
			controller.triggerViewSplitMode(e);
		});
		viewMenu.add(menuItemSplit);

	}

	public void setController(PController controller) {
		this.controller = controller;
	}

	public JLabel getStatusLabel() {
		return statusLabel;
	}

	public PComponent getPhototequeComponent() {
		return component;
	}
	
	public void removePhototequeComponent() {
		centralPanel.remove(componentBox);
//		updateEditButtonsStates(false);
		setFormatButtonsEnabled(false);
		setTextAnnotationButtonsEnabled(false);
		phototequeFrame.repaint();
	}

	private void setFormatButtonsEnabled(boolean enabled) {
		cursorToggleButton.setEnabled(enabled);
		handToggleButton.setEnabled(enabled);
		phototequeFrame.repaint();
	}
	
	private void setTextAnnotationButtonsEnabled(boolean enabled) {
		
		fontToggleButton.setEnabled(enabled);
		boldToggleButton.setEnabled(enabled);
		underlineToggleButton.setEnabled(enabled);
		setAnnotationButtonsEnabled(enabled);
	}
	
	private void setAnnotationButtonsEnabled(boolean enabled) {
		brushButton.setEnabled(enabled);
		colorChooser.setEnabled(enabled);
	}

	public void setComponent(String filePath) {
		if (centralPanel.getComponentCount() > 0)
			centralPanel.removeAll();
		component = new PComponent(this, filePath);
	
		componentBox = new Box(BoxLayout.LINE_AXIS);
		componentBox.add(Box.createHorizontalGlue());
		componentBox.add(component);
		centralPanel.add(Box.createVerticalGlue());
		centralPanel.add(componentBox);
		setFormatButtonsEnabled(true);
		
//		centralPanel.add(component);
		
		phototequeFrame.repaint();
	}
	
	public void setAnnotationButtons(Annotation.type annotationType) {
		if (annotationType == Annotation.type.TEXT) {
			setTextAnnotationButtonsEnabled(true);
		} else {
			setAnnotationButtonsEnabled(true);
		}
	}
	
	public void resetAnnotationButtons() {
		setTextAnnotationButtonsEnabled(false);
	}
	
	public JPanel getPane() {
		return topLevelPanel;
	}
	
}
