package upsud.hci909.lab2;

public class Phototeque {

	public static void main(String[] args) {
		PhototequeModel ptmodel = new PhototequeModel();
		PhototequeView ptview = new PhototequeView(ptmodel);
		PhototequeController ptcontroller = new PhototequeController(ptmodel, ptview);
		ptview.setController(ptcontroller);
	}

}
