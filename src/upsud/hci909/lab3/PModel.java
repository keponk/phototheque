package upsud.hci909.lab3;

import java.awt.Color;
import java.awt.Dimension;

public class PModel {
	
	private final int defaultFrameWidth = 1200;
	private final int defaultFrameHeight = 800;
	private String statusMessage;
	private Color backgroundColor = new Color(255, 235, 255);
	
	public final String TITLE = "Phototeque Lab 3"; 
	public final class UpdateMessages {
		public static final String HOME_CLICK = "Home button clicked.";
		public static final String BACK_CLICK = "Back button clicked.";
		public static final String FORWARD_CLICK = "Forward button clicked.";
		public static final String TOGGLE_CLICK = "Toggle button clicked.";
		public static final String UNSUPPORTED_FILE = "File type is not supported (yet).";
		public static final String DELETE_FILE = "Delete button clicked.";
	}
	
	public Dimension getFrameDesfaultSize() {
		return new Dimension(defaultFrameWidth, defaultFrameHeight);
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String message) {
		statusMessage = message;
	}
	public Color getBackgroundColor() {
		return backgroundColor;
	}
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

}
