package upsud.hci909.lab2;

import java.awt.Point;

public class PhototequeTextEntry {
	private Point point;
	private String text;
	private String font;
	
	public PhototequeTextEntry(Point cursorLocation, String text) {
		this.point = cursorLocation;
		this.text = text;
//		this.font = font;
	}
	public Point getPoint() {
		return point;
	}
	public void setPoint(Point cursorLocation) {
		this.point = cursorLocation;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getFont() {
		return font;
	}
	public void setFont(String font) {
		this.font = font;
	}
}
